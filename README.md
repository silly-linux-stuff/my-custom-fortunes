# Custom fortunes
These are my custom fortunes for the UNIX `fortune` command.

## Installation:
1. Clone repo / download as zip
2. Execute `strfile` on every extension-less text-file using `strfile name-of-file`
2. Move all files to your fortunes folder (`/usr/share/fortune` in my case. run `sudo find / -type d -name "fortune"` to find it.) It's the folder with extension-less files and corresponding `.dat`-files. Run `sudo cp ./* /your/fortune/folder` to copy.

## Use
Run the new fortunes using `fortune name-of-new-fortune`  
E.g. `fortune internet-memes`